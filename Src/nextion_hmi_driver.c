/*
 * nextion_hmi_driver.c
 *
 *  Created on: Jul 29, 2020
 *      Author: marci
 */


/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include "nextion_hmi_driver.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/
#define NEXTION_USART						USART1
#define NEXTION_USART_IRQ_NUMBER			USART1_IRQn
#define NEXTION_USART_IRQ_PRIORITY			15

#define NEXTION_TX_PIN						GPIO_PIN_6
#define NEXTION_RX_PIN						GPIO_PIN_7
#define NEXTION_GPIO_PORT					GPIOB
#define NEXTION_GPIO_ALT_FUNC				7

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/
static void Nextion_usartInit();
static void USART_GPIOInit(void);
static void appendEndOfCommandToMsg(uint8_t * cmd, uint8_t cmdLen);
static void processMsg(const uint8_t * textObjectName, const uint8_t * cmd, uint8_t * value);
static void processChangeTextMsg(const uint8_t * textObjectName, const uint8_t * cmd, uint8_t * value);
/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/
// data to handle uart communication
USART_Handle_t nextion_usart = {0};

// WARNING: Below variables should not be changed.
// They are required to Nextion commands work properly.
// Each Nextion uart command must end with 0xFF 0xFF 0xFF
const static volatile uint8_t Nextion_CommandEnd[3] = {0xff, 0xff, 0xff};
const static uint8_t Nextion_CommandEndSize = 3;

// WARNING END

/* Nextion objects and atributes defines */

// Text
static const uint8_t NextionTextChangeText_cmd[6] = ".txt=\"";
static const uint8_t NextionTextChangeFont_cmd[6] = ".font=";
static const uint8_t NextionTextChangeFontColor_cmd[5] = ".pco=";
static const uint8_t NextionTextChangeBackgroundColor_cmd[5] = ".bco=";

// Picture
static const uint8_t NextionPictureChangePicture_cmd[5] = ".pic=";


/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void Nextion_driverConstructor() {

	USART_GPIOInit();
    Nextion_usartInit();
}

void Nextion_send_cmd(uint8_t * cmd, uint8_t cmdLen) {

    uint8_t msgLen = cmdLen + Nextion_CommandEndSize;
    uint8_t msgToSend[msgLen];

    memcpy(msgToSend, cmd, cmdLen);
    appendEndOfCommandToMsg(msgToSend, cmdLen);

    USART_SendData(&nextion_usart, msgToSend, msgLen);
}

void Nextion_ModifyText(const uint8_t * textObjectName, nextion_text_operations_t operation, uint8_t * value ){

	switch(operation){
	case NextionTextChangeText:
		processChangeTextMsg(textObjectName, NextionTextChangeText_cmd, value);
	break;
	case NextionTextChangeFont:
		processMsg(textObjectName, NextionTextChangeFont_cmd, value);
	break;
	case NextionTextChangeFontColor:
		processMsg(textObjectName, NextionTextChangeFontColor_cmd, value);
	break;
	case NextionTextChangeBackgroundColor:
		processMsg(textObjectName, NextionTextChangeBackgroundColor_cmd, value);
	break;
	default:
	break;
	}
}

void Nextion_ModifyPicture(const uint8_t * pictureObjectName, nextion_picture_operations_t operation, uint8_t * value ){
	switch(operation){
	case NextionPictureChangePicture:
		processMsg(pictureObjectName, NextionPictureChangePicture_cmd, value);
	break;
	default:
	break;
	}
}

void Nextion_ModifyWaveform(uint8_t * pictureObjectName, nextion_picture_operations_t operation, uint8_t * value ){
	switch(operation){
	case NextionPictureChangePicture:
		processMsg(pictureObjectName, NextionPictureChangePicture_cmd, value);
	break;
	default:
	break;
	}
}

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static inline void appendEndOfCommandToMsg(uint8_t * cmd, uint8_t cmdLen) {

    for (uint8_t i = 0; i < Nextion_CommandEndSize; i++) {
        cmd[i + cmdLen] = Nextion_CommandEnd[i];
    }
}

static void processMsg(const uint8_t * textObjectName, const uint8_t * cmd, uint8_t * value){

	uint8_t objectNameLen = strlen(textObjectName);
	uint8_t commandLen = strlen(cmd);
	uint8_t valueLen = strlen(value);

	uint8_t msgLen = commandLen + objectNameLen + valueLen;
	uint8_t msgToSend[msgLen];

	memcpy(msgToSend, textObjectName, objectNameLen);

	for(uint8_t i = 0; i < commandLen; i++){
		msgToSend[i + objectNameLen] = cmd[i];
	}
	for(uint8_t i = 0; i < valueLen; i++){
		msgToSend[i + objectNameLen + commandLen] = value[i];
	}
	Nextion_send_cmd(msgToSend, msgLen);
}

static void processChangeTextMsg(const uint8_t * textObjectName, const uint8_t * cmd, uint8_t * value){

	uint8_t objectNameLen = strlen(textObjectName);
	uint8_t commandLen = strlen(cmd);
	uint8_t valueLen = strlen(value);

	uint8_t msgLen = commandLen + objectNameLen + valueLen + 1;
	uint8_t msgToSend[msgLen];

	memcpy(msgToSend, textObjectName, objectNameLen);

	for(uint8_t i = 0; i < commandLen; i++){
		msgToSend[i + objectNameLen] = cmd[i];
	}
	for(uint8_t i = 0; i < valueLen; i++){
		msgToSend[i + objectNameLen + commandLen] = value[i];
	}
	msgToSend[msgLen - 1] = 34;
	Nextion_send_cmd(msgToSend, msgLen);
}

static void USART_GPIOInit(void){

	GPIO_Handle_t USARTgpio = {0};

	USARTgpio.GPIO_PinConfig.pGPIOx = NEXTION_GPIO_PORT;
	USARTgpio.GPIO_PinConfig.GPIO_PinMode = GPIO_MODE_AF;
	USARTgpio.GPIO_PinConfig.GPIO_PinAltFunMode = NEXTION_GPIO_ALT_FUNC;
	USARTgpio.GPIO_PinConfig.GPIO_PinOPType = GPIO_OP_TYPE_PP;
	USARTgpio.GPIO_PinConfig.GPIO_PinPuPdControl = GPIO_PIN_PULL_UP;
	USARTgpio.GPIO_PinConfig.GPIO_PinSpeed = GPIO_SPEED_HIGH;
	USARTgpio.numberOfPinsToConfig = 2;
	//TX
	USARTgpio.PinNumber[0] = NEXTION_TX_PIN;
	//RX
	USARTgpio.PinNumber[1] = NEXTION_RX_PIN;
	GPIO_Init(&USARTgpio);
}

static void Nextion_usartInit(){

	nextion_usart.pUSARTx = NEXTION_USART;
	nextion_usart.USART_Config.USART_Baud = USART_STD_BAUD_115200;
	nextion_usart.USART_Config.USART_HWFlowControl = USART_HW_FLOW_CTRL_NONE;
	nextion_usart.USART_Config.USART_Mode = USART_MODE_TXRX;
	nextion_usart.USART_Config.USART_NoOfStopBits = USART_STOPBITS_1;
	nextion_usart.USART_Config.USART_ParityControl = USART_PARITY_DISABLE;
	nextion_usart.USART_Config.USART_WordLength = USART_WORDLEN_8BITS;

	NVIC_IRQInterruptConfig(NEXTION_USART_IRQ_NUMBER, ENABLE);
	NVIC_IRQPriorityConfig(NEXTION_USART_IRQ_NUMBER, NEXTION_USART_IRQ_PRIORITY);

	USART_Init(&nextion_usart);

	USART_PeripheralControl(NEXTION_USART, ENABLE);
}

/*******************************************************************************
 *    HARDWARE CALLBACKS
 ******************************************************************************/

void USART1_EXTI25_IRQHandler(void){

	USART_IRQHandling(&nextion_usart);
}




/*******************************************************************************
 *    end of file
 ******************************************************************************/









/*
 * nextion_hmi_driver.h
 *
 *  Created on: Jul 29, 2020
 *      Author: marci
 */

#ifndef NEXTION_HMI_DRIVER_H_
#define NEXTION_HMI_DRIVER_H_

/*******************************************************************************
 *    Description:   This simple library was written to achieve convienient
 *    communication with Nextion HMIs. Library has only few functionalities
 *    ( only those which was important for my other projects), but will
 *    be developed in the future.
 *
 *    Library also contains simple Ceedling uint tests.
 *
 ******************************************************************************/


/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/
#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "stm32f303xx.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/
typedef enum{
	NextionTextChangeText,
	NextionTextChangeFont,
	NextionTextChangeFontColor,
	NextionTextChangeBackgroundColor
}nextion_text_operations_t;

typedef enum{
	NextionPictureChangePicture
}nextion_picture_operations_t;

typedef enum{
	NextionAddToWaveform
}nextion_waveform_operations_t;

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

void Nextion_driverConstructor();

void Nextion_ModifyText(const uint8_t * textObjectName, nextion_text_operations_t operation, uint8_t * value  );
void Nextion_ModifyPicture(const uint8_t * pictureObjectName, nextion_picture_operations_t operation, uint8_t * value );

void Nextion_send_cmd(uint8_t * cmd, uint8_t cmdLen); // Public only for Ceedling testig purposes

#endif /* NEXTION_HMI_DRIVER_H_ */

/*******************************************************************************
 *    end of file
 ******************************************************************************/



#include "nextion_hmi_driver.h"

/*
 * Simple example program showing how to use
 * NextionHMI_Library. All USART peripheral
 * initialization is performed in driver constructor.
 * To change peripheral configuration refer to
 * library source file.
 */

const uint8_t textObjectName[4] = "txt\0";
const uint8_t textOnScreen[13] = "textOnScreen\0";

int main(void)
{

	Nextion_driverConstructor();

	Nextion_ModifyText(textObjectName,
			NextionTextChangeText, textOnScreen);

	for(;;);
}

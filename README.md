This simple library was written to achieve convienient
communication with Nextion HMIs. Library has only few functionalities
( only those which was important for my other projects), but will
be developed in the future.

Library also contains simple Ceedling uint tests. 

Library use drivers from other repository on my git. Drivers can be 
easly replaced by any other, for example STM HAL. Note that replacing 
drivers must be followed by changes in unit test if You want them to 
work properly.

/*
 * test_nextion_hmi_driver.c
 *
 *  Created on: 8 sie 2020
 *      Author: marci
 */


/*
 * test_temperatureService.c
 *
 *  Created on: 8 sie 2020
 *      Author: marci
 */

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

 //-- unity: unit test framework
#include "unity.h"

//-- module being tested: messageService
#include "nextion_hmi_driver.h"

//-- mocks
#include "mock_stm32f303xx_uart.h"
#include "mock_stm32f303xx_gpio.h"
#include "mock_stm32f303xx_system.h"

//-- external libraries
#include <string.h>
#include <stdio.h>

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
*    PRIVATE TYPES
******************************************************************************/

/*******************************************************************************
*    PRIVATE DATA
******************************************************************************/

/*******************************************************************************
*    PRIVATE FUNCTIONS
******************************************************************************/


/*******************************************************************************
*    SETUP, TEARDOWN
******************************************************************************/

void setUp(void)
{
	GPIO_Init_Ignore();
	NVIC_IRQInterruptConfig_Ignore();
	NVIC_IRQPriorityConfig_Ignore();
	USART_Init_Ignore();
	USART_PeripheralControl_Ignore();
	Nextion_driverConstructor();


}

void tearDown(void)
{

}

/*******************************************************************************
 *    TESTS
 ******************************************************************************/

// Nextion_send_cmd TEST

void test_cmdSendedWithCommandEndSequence(){

	uint8_t expect[6] = "123";
	expect[3] = 0xFF;
	expect[4] = 0xFF;
	expect[5] = 0xFF;

	uint8_t size = strlen(expect);

	USART_SendData_ExpectWithArray(NULL, 1, expect, size, size);
	USART_SendData_IgnoreArg_pUSARTHandle();

	Nextion_send_cmd("123", 3);
}

void test_cmdSendedWithCommandEndSequenceSecondTest(){

	uint8_t expect[7] = "Aba.";
	expect[4] = 0xFF;
	expect[5] = 0xFF;
	expect[6] = 0xFF;

	uint8_t size = strlen(expect);

	USART_SendData_ExpectWithArray(NULL, 1, expect, size, size);
	USART_SendData_IgnoreArg_pUSARTHandle();

	Nextion_send_cmd("Aba.", 4);
}

// modifyText TESTS

void test_modifyTextSendArgs(){

	uint8_t expect[14] = "p.txt=\"tak\"";
	expect[11] = 0xFF;
	expect[12] = 0xFF;
	expect[13] = 0xFF;

	uint8_t size = strlen(expect);

	USART_SendData_ExpectWithArray(NULL, 1, expect, size, size);
	USART_SendData_IgnoreArg_pUSARTHandle();

	Nextion_ModifyText("p", NextionTextChangeText, "tak");
}

// modifyPicture TESTS

void test_modifyPictureSendArgs(){

	uint8_t expect[14] = "tee.pic=27a";
	expect[11] = 0xFF;
	expect[12] = 0xFF;
	expect[13] = 0xFF;

	uint8_t size = strlen(expect);

	USART_SendData_ExpectWithArray(NULL, 1, expect, size, size);
	USART_SendData_IgnoreArg_pUSARTHandle();

	Nextion_ModifyPicture("tee", NextionPictureChangePicture, "27a");

}

/*******************************************************************************
 *    end of file
 ******************************************************************************/

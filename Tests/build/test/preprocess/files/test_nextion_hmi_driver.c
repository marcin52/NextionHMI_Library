#include "build/temp/_test_nextion_hmi_driver.c"
#include "build/test/mocks/mock_stm32f303xx_system.h"
#include "build/test/mocks/mock_stm32f303xx_gpio.h"
#include "build/test/mocks/mock_stm32f303xx_uart.h"
#include "C:/Users/marci/Desktop/Programowanie/INNOVAQUATIC/NextionLibraryDevelop/Src/nextion_hmi_driver.h"
#include "C:/Ruby27-x64/lib/ruby/gems/2.7.0/gems/ceedling-0.30.0/vendor/unity/src/unity.h"








void setUp(void)

{

 GPIO_Init_CMockIgnore();

 NVIC_IRQInterruptConfig_CMockIgnore();

 NVIC_IRQPriorityConfig_CMockIgnore();

 USART_Init_CMockIgnore();

 USART_PeripheralControl_CMockIgnore();

 Nextion_driverConstructor();





}



void tearDown(void)

{



}















void test_cmdSendedWithCommandEndSequence(){



 uint8_t expect[6] = "123";

 expect[3] = 0xFF;

 expect[4] = 0xFF;

 expect[5] = 0xFF;



 uint8_t size = strlen(expect);



 USART_SendData_CMockExpectWithArray(88, 

((void *)0)

, 1, expect, size, size);

 USART_SendData_CMockIgnoreArg_pUSARTHandle(89);



 Nextion_send_cmd("123", 3);

}



void test_cmdSendedWithCommandEndSequenceSecondTest(){



 uint8_t expect[7] = "Aba.";

 expect[4] = 0xFF;

 expect[5] = 0xFF;

 expect[6] = 0xFF;



 uint8_t size = strlen(expect);



 USART_SendData_CMockExpectWithArray(103, 

((void *)0)

, 1, expect, size, size);

 USART_SendData_CMockIgnoreArg_pUSARTHandle(104);



 Nextion_send_cmd("Aba.", 4);

}







void test_modifyTextSendArgs(){



 uint8_t expect[14] = "p.txt=\"tak\"";

 expect[11] = 0xFF;

 expect[12] = 0xFF;

 expect[13] = 0xFF;



 uint8_t size = strlen(expect);



 USART_SendData_CMockExpectWithArray(120, 

((void *)0)

, 1, expect, size, size);

 USART_SendData_CMockIgnoreArg_pUSARTHandle(121);



 Nextion_ModifyText("p", NextionTextChangeText, "tak");

}







void test_modifyPictureSendArgs(){



 uint8_t expect[14] = "tee.pic=27a";

 expect[11] = 0xFF;

 expect[12] = 0xFF;

 expect[13] = 0xFF;



 uint8_t size = strlen(expect);



 USART_SendData_CMockExpectWithArray(137, 

((void *)0)

, 1, expect, size, size);

 USART_SendData_CMockIgnoreArg_pUSARTHandle(138);



 Nextion_ModifyPicture("tee", NextionPictureChangePicture, "27a");



}

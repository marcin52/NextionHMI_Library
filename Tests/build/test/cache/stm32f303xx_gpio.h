#include "C:/Users/marci/Desktop/Programowanie/INNOVAQUATIC/NextionLibraryDevelop/drivers/Inc/stm32f303xx.h"


typedef struct

{

 GPIO_RegDef_t *pGPIOx;





 uint8_t GPIO_PinMode;

 uint8_t GPIO_PinSpeed;

 uint8_t GPIO_PinPuPdControl;

 uint8_t GPIO_PinOPType;

 uint8_t GPIO_PinAltFunMode;



}GPIO_PinConfig_t;





typedef struct{



 uint8_t PinNumber[15];







 uint8_t numberOfPinsToConfig;



 GPIO_PinConfig_t GPIO_PinConfig;



}GPIO_Handle_t;

void GPIO_PeriphClockControl(GPIO_RegDef_t *pGPIOx, uint8_t ENorDI);









void GPIO_Init(GPIO_Handle_t *pGPIO);

void GPIO_DeInit(GPIO_RegDef_t *pGPIOx);











uint8_t GPIO_ReadPin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);

uint16_t GPIO_ReadPort(GPIO_RegDef_t *pGPIOx);

void GPIO_WritePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber, uint8_t value);

void GPIO_WritePort(GPIO_RegDef_t *pGPIOx, uint16_t value);

void GPIO_TogglePin(GPIO_RegDef_t *pGPIOx, uint8_t PinNumber);









void GPIO_IRQHandling(uint8_t PinNumber);

void GPIO_ClearEXTI_InterruptPendingFlag(uint8_t PinNumber);

#include "C:/Users/marci/Desktop/Programowanie/INNOVAQUATIC/NextionLibraryDevelop/drivers/Inc/stm32f303xx.h"
typedef struct

{

 uint8_t USART_Mode;

 uint32_t USART_Baud;

 uint8_t USART_NoOfStopBits;

 uint8_t USART_WordLength;

 uint8_t USART_ParityControl;

 uint8_t USART_HWFlowControl;

}USART_Config_t;











typedef struct

{

 USART_RegDef_t *pUSARTx;

 USART_Config_t USART_Config;

 uint8_t *pTxBuffer;

 uint8_t *pRxBuffer;

 uint32_t TxLen;

 uint32_t RxLen;

 uint8_t TxBusyState;

 uint8_t RxBusyState;

}USART_Handle_t;

void USART_PeriphClockControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable);









void USART_Init(USART_Handle_t *pUSARTHandle);

void USART_DeInit(USART_Handle_t *pUSARTHandle);









void USART_SendData(USART_Handle_t *pUSARTHandle, uint8_t *pTxBuffer, uint32_t Len);

void USART_ReceiveData(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len);

uint8_t USART_SendDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t Len);

uint8_t USART_ReceiveDataIT(USART_Handle_t *pUSARTHandle,uint8_t *pRxBuffer, uint32_t Len);

uint8_t USART_StartSendingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel);

uint8_t USART_StartReceivingDataDMA(USART_Handle_t *pUSARTHandle, DMA_Channel_RegDef_t * dmaChannel);

uint8_t USART_SendDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);

uint8_t USART_ReceiveDataDMA(USART_Handle_t *pUSARTHandle,uint8_t *pTxBuffer, uint32_t len, DMA_Channel_RegDef_t * dmaChannel);







void USART_IRQHandling(USART_Handle_t *pUSARTHandle);











uint8_t USART_GetFlagStatus(USART_RegDef_t *pUSARTx, uint8_t StatusFlagName);

void USART_ClearFlag(USART_RegDef_t *pUSARTx, uint16_t StatusFlagName);

void USART_PeripheralControl(USART_RegDef_t *pUSARTx, uint8_t EnableOrDisable);

void USART_SetBaudRate(USART_RegDef_t *pUSARTx, uint32_t BaudRate);











void USART_TransferCpltCallback(USART_Handle_t *pUSARTHandle);

void USART_ReadDataRegisterEmptyCallback(USART_Handle_t *pUSARTHandle);

void USART_CTS_EventCallback(USART_Handle_t *pUSARTHandle);

void USART_IdleEventCallback(USART_Handle_t *pUSARTHandle);

void USART_OverrunErrorCallback(USART_Handle_t *pUSARTHandle);

void USART_NoiseErrorCallback(USART_Handle_t *pUSARTHandle);

void USART_FramingErrorCallback(USART_Handle_t *pUSARTHandle);

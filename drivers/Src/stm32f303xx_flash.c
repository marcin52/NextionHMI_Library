/*
 * stm32f303xx_flash.c
 *
 *  Created on: 23 lip 2020
 *      Author: marci
 */

#include "stm32f303xx_flash.h"

void FLASH_SetFlashLatency(FLASH_LATENCY_WS_typedef latency){
	if( latency == FLASH_LATENCY_TWO_WS){
		FLASH->ACR |= FLASH_ACR_LATENCY_2;
	} else if( latency == FLASH_LATENCY_ONE_WS){
		FLASH->ACR |= FLASH_ACR_LATENCY_1;
	} else{
		FLASH->ACR |= FLASH_ACR_LATENCY_0;
	}
}

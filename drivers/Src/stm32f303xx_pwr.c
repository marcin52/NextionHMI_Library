/*
 * stm32f303xx_pwr.c
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#ifndef SRC_STM32F303XX_PWR_C_
#define SRC_STM32F303XX_PWR_C_

#include "stm32f303xx_pwr.h"

void PWR_RTC_DomainWriteProtectionControl(uint8_t EnableOrDisable){

	if(EnableOrDisable == DISABLE){
		PWR->CR |= (1 << PWR_CR_DBP_Pos);
	}else {
		PWR->CR &= ~(1 << PWR_CR_DBP_Pos);
	}
}


#endif /* SRC_STM32F303XX_PWR_C_ */

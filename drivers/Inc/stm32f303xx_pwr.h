/*
 * stm32f303xx_pwr.h
 *
 *  Created on: 26 lip 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_PWR_H_
#define INC_STM32F303XX_PWR_H_

#include "stm32f303xx.h"

#define PWR_PCKL_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_PWREN_Pos))

#define PWR_PCKL_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_PWREN_Pos))

void PWR_RTC_DomainWriteProtectionControl(uint8_t EnableOrDisable);

#endif /* INC_STM32F303XX_PWR_H_ */

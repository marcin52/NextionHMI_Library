/*
 * stm32f303xx_system.h
 *
 *  Created on: 4 maj 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_SYSTEM_H_
#define INC_STM32F303XX_SYSTEM_H_

#include "stm32f303xx.h"
#include "core_cm4.h"

/*
 * SysTick Control and Status Register bit positions
 */
#define SYST_CSR_COUNTFLAG_BIT_POS				16
#define SYST_CSR_CLOCKSOURCE_BIT_POS			2
#define SYST_CSR_TICKINT_BIT_POS				1
#define SYST_CSR_ENABLE_BIT_POS					0

/*
 * SysTick Reload Value Register bit positions
 */
#define SYST_RVR_RELOAD_BIT_POS					0

/*
 * SysTick Current Value Register bit positions
 */
#define SYST_CVR_CURRENT_BIT_POS				0
#define SYST_CVR_RESERVED_BIT_POS				24

/*
 * SysTick Calibration Register bit positions
 */
#define SYST_CALIB_NOREF_BIT_POS				31
#define SYST_CALIB_SKEW_BIT_POS					30
#define SYST_CALIB_TENMS_BIT_POS				0

/*
 * @Systick_ClockSource
 */
#define SYST_CLOCKSOURCE_PROCESSOR				1
#define SYST_CLOCKSOURCE_EXTERNAL				0

/*
 * SysTick Current Tick Value
 */
#define SYST_CVR_CURRENT_MASK					0x00FFFFFF
#define SYST_CVR_RESERVED_MASK					0xFF000000

/*
 * some generic macros
 */
#define SYST_1kHZ_FREQ 							1000
#define SYST_100HZ_FREQ 						100
#define SYST_10HZ_FREQ 							10
#define SYST_1HZ_FREQ 							1

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * SysTick Init
 */
void SysTick_Init(void);
void SysTick_StartTick(void);
void SysTick_Configure_EnableSysTickException(void);
void SysTick_Configure_ClockSource(uint8_t clockSource);
void SysTick_Configure_ReloadValue(uint32_t reload);
void SysTick_Configure_CalibrationValue(uint32_t calibrationValue);

/*
 * Other SysTick control APIs
 */
void SysTick_ClearCurrentTickValue(void);
uint32_t SysTick_GetCurrentTickValue(void);

/*
 * IRQ Configuration and ISR handling
 */

void NVIC_IRQInterruptConfig(uint8_t IRQNumber, uint8_t EnableOrDisable);
void NVIC_IRQPriorityConfig(uint8_t IRQNumber, uint32_t IRQpriority);
void SysTick_IRQHandling(void);

/*
 * Floating Point Unit Control
 */

void FPU_EnableFullControl();
void FPU_DisableFullControl();

#endif /* INC_STM32F303XX_SYSTEM_H_ */

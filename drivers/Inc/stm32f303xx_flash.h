/*
 * stm32f303xx_flash.h
 *
 *  Created on: 23 lip 2020
 *      Author: marci
 */

#ifndef SRC_STM32F303XX_FLASH_H_
#define SRC_STM32F303XX_FLASH_H_

#include "stm32f303xx.h"

typedef enum{
	FLASH_LATENCY_ZERO_WS,
	FLASH_LATENCY_ONE_WS,
	FLASH_LATENCY_TWO_WS,
}FLASH_LATENCY_WS_typedef;

void FLASH_SetFlashLatency(FLASH_LATENCY_WS_typedef latency);


#endif /* SRC_STM32F303XX_FLASH_H_ */

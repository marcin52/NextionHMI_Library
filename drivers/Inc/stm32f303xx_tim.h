/*
 * stm32f303xx_tim.h
 *
 *  Created on: Apr 18, 2020
 *      Author: marci
 */

#ifndef INC_STM32F303XX_TIM_H_
#define INC_STM32F303XX_TIM_H_

#include "stm32f303xx.h"

typedef struct{
	uint16_t prescaler;							/*! Number from 0x1 to 0xFFFF			 	 */
	uint16_t autoReloadRegister;				/*! Number from 0x1 to 0xFFFF			 	 */
	uint8_t onePulseMode;						/*! Possible values: ENABLE, DISABLE		 */
	uint8_t softwareCounterUpdateGeneration;	/*! Possible values: ENABLE, DISABLE		 */
	uint8_t TIM_DMARequestEnable;				/*! Possible values: ENABLE, DISABLE		 */
	uint8_t direction;							/*! Possible values: refer to @TimDirection  */
}TIM_TimeBaseInit_t;

typedef struct{
	uint8_t timChannel;							/*! Possible values refer to @TIM_CHANNELS	  */
	uint8_t outputMode;							/*! Possible values refer to @Tim_OutputMode  */
	TIM_TimeBaseInit_t timeBase;
}TIM_OutputCompareInit_t;


/*
 * @TimDirection
 */
#define TIM_DIRECTION_UP_COUNTING		0
#define TIM_DIRECTION_DOWN_COUNTING		1

/*
 * @TIM_CHANNELS
 */
#define TIM_CHANNEL_1					1
#define TIM_CHANNEL_2					2
#define TIM_CHANNEL_3					3
#define TIM_CHANNEL_4					4
#define TIM_CHANNEL_5					5
#define TIM_CHANNEL_6					6

/*
 * @Tim_OutputMode
 */
#define TIM_MODE_OUTPUT_COMPARE_1		1
#define TIM_MODE_OUTPUT_COMPARE_2		2
#define TIM_MODE_OUTPUT_COMPARE_TOGGLE	3
#define TIM_MODE_PWM_1					6
#define TIM_MODE_PWM_2					7

/*
 * @CaptureCompareSelection
 */

#define TIM_CAPTURE_COMPARE_SELECTION_OUTPUT		0
#define TIM_CAPTURE_COMPARE_SELECTION_INPUT_TI1		1
#define TIM_CAPTURE_COMPARE_SELECTION_INPUT_TI2		2
#define TIM_CAPTURE_COMPARE_SELECTION_INPUT_TRS		3
/******************************************************************************************
 *								Macros supported by this driver
 *		 		These macros can be used to do low some low-level operations
 ******************************************************************************************/


/*
 * Clock Enable Macros for Timer peripherals
 */

#define TIM1_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM1EN_Pos))
#define TIM8_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM8EN_Pos))
#define TIM15_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM15EN_Pos))
#define TIM16_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM16EN_Pos))
#define TIM17_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM17EN_Pos))
#define TIM20_PCLK_EN()		(RCC->APB2ENR |= (1 << RCC_APB2ENR_TIM20EN_Pos))

#define TIM2_PCLK_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_TIM2EN_Pos))
#define TIM3_PCLK_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_TIM3EN_Pos))
#define TIM4_PCLK_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_TIM4EN_Pos))
#define TIM6_PCLK_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_TIM6EN_Pos))
#define TIM7_PCLK_EN()		(RCC->APB1ENR |= (1 << RCC_APB1ENR_TIM7EN_Pos))

/*
 * Clock Disable Macros for Timer peripherals
 */

#define TIM1_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM1EN_Pos))
#define TIM8_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM8EN_Pos))
#define TIM15_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM15EN_Pos))
#define TIM16_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM16EN_Pos))
#define TIM17_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM17EN_Pos))
#define TIM20_PCLK_DI()		(RCC->APB2ENR &= ~(1 << RCC_APB2ENR_TIM20EN_Pos))

#define TIM2_PCLK_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_TIM2EN_Pos))
#define TIM3_PCLK_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_TIM3EN_Pos))
#define TIM4_PCLK_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_TIM4EN_Pos))
#define TIM6_PCLK_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_TIM6EN_Pos))
#define TIM7_PCLK_DI()		(RCC->APB1ENR &= ~(1 << RCC_APB1ENR_TIM7EN_Pos))

/*
 * generic macros
 */

#define TIM_GET_CCMR_REG(x, TIM)			   ((x == TIM_CHANNEL_1)?&(TIM->CCMR1):\
												(x == TIM_CHANNEL_2)?&(TIM->CCMR1):\
												(x == TIM_CHANNEL_3)?&(TIM->CCMR2):\
												(x == TIM_CHANNEL_4)?&(TIM->CCMR2):\
												(x == TIM_CHANNEL_5)?&(TIM->CCMR3):\
												(x == TIM_CHANNEL_6)?&(TIM->CCMR3):0)

#define TIM_GER_CCR_REG(x, TIM)				   ((x == TIM_CHANNEL_1)?&(TIM->CCR1):\
												(x == TIM_CHANNEL_2)?&(TIM->CCR2):\
												(x == TIM_CHANNEL_3)?&(TIM->CCR3):\
												(x == TIM_CHANNEL_4)?&(TIM->CCR4):\
												(x == TIM_CHANNEL_5)?&(TIM->CCR5):\
												(x == TIM_CHANNEL_6)?&(TIM->CCR6):0)



#define TIM_GET_CCMR_BIT_SHIFT(x)			   ((x == TIM_CHANNEL_1)?0:\
												(x == TIM_CHANNEL_2)?8:\
												(x == TIM_CHANNEL_3)?0:\
												(x == TIM_CHANNEL_4)?8:\
												(x == TIM_CHANNEL_5)?0:\
												(x == TIM_CHANNEL_6)?8:33)



/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * Timer periph clock control
 */
void TIM_PeriphClockControl(TIM_RegDef_t *pTIMx, uint8_t EnableOrDisable);

/*
 * Timer Init and Deinit
 */
void TIM_TimeBaseInit(TIM_RegDef_t *Timer, TIM_TimeBaseInit_t *pTIMh);
void TIM_TimeBaseDeInit(TIM_RegDef_t *Timer);
void TIM_OutputModeInit(TIM_RegDef_t *Timer, TIM_OutputCompareInit_t *pTIMh);
void TIM_OutputModeDeInit(TIM_RegDef_t *Timer);
void TIM_StartCounting(TIM_RegDef_t *Timer);
void TIM_StopCounting(TIM_RegDef_t *Timer);

/*
 * Timer output compare configuration
 */

void TIM_ConfigureOutputMode(TIM_RegDef_t *Timer, TIM_OutputCompareInit_t *pTIMh);
void TIM_ConfigureChannelAsOutput(TIM_RegDef_t *Timer, uint8_t timChannel);
void TIM_SetCompareValue(TIM_RegDef_t *Timer, uint8_t timChannel, uint16_t compareValue);
uint16_t TIM_GetCompareValue(TIM_RegDef_t *Timer, uint8_t timChannel);
void TIM_EnableCaptureCompare(TIM_RegDef_t *Timer, uint8_t timChannel);
void TIM_EnableOutputComparePreload(TIM_RegDef_t *Timer, uint8_t timChannel);
void TIM_EnableMainOutput(TIM_RegDef_t *Timer);

/*
 * Timer time base configuration
 */

void TIM_SetPrescaler(TIM_RegDef_t *Timer, uint16_t prescaler);
void TIM_SetAutoReload(TIM_RegDef_t *Timer, uint16_t autoReload);
void TIM_Configure_OnePulseMode(TIM_RegDef_t *Timer, uint8_t EnableOrDisable);
void TIM_Configure_SoftwareUpdateMode(TIM_RegDef_t *Timer, uint8_t EnableOrDisable);
void TIM_Configure_DMARequest(TIM_RegDef_t *Timer, uint8_t EnableOrDisable);
void TIM_Configure_Interrupt(TIM_RegDef_t *Timer, uint8_t EnableOrDisable);
void TIM_Configure_AutoReloadPreload(TIM_RegDef_t *Timer, uint8_t EnableOrDisable);
void TIM_Configure_CountDirection(TIM_RegDef_t *Timer, uint8_t countDirection);

/*
 * IRQ Configuration and ISR handling
 */

void TIM_IRQHandling(TIM_RegDef_t *Timer);

/*
 * Other Peripheral Control APIs
 */

void TIM_GenerateRegistersUpdate(TIM_RegDef_t *Timer);
void TIM_GenerateUpdateEvent(TIM_TimeBaseInit_t *pTIMh);
void TIM_SetCounterValue(TIM_RegDef_t *Timer, uint16_t counterValue);
uint16_t TIM_GetCounterValue(TIM_RegDef_t *Timer);
void TIM_ClearUpdateInterruptFlag(TIM_RegDef_t *Timer);
uint8_t TIM_GetUpdateStatus(TIM_RegDef_t *Timer);

/*
 * Weak application callbacks
 */

__weak void TimerUpdateEventCallback(TIM_RegDef_t *Timer);

#endif /* INC_STM32F303XX_TIM_H_ */

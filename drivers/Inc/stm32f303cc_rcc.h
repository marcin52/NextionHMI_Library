/*
 * stm32f303cc_rcc.h
 *
 *  Created on: Apr 18, 2020
 *      Author: marci
 */

#ifndef INC_STM32F303CC_RCC_H_
#define INC_STM32F303CC_RCC_H_

#include "stm32f303xx.h"

typedef struct{
	uint32_t* peripheralBaseAddress;
	uint8_t clockSource;
	uint8_t clockPrescaler;
}PeriperalClockSource_t;

/*
 * @Oscillators_definition
 */
#define OSCILLATOR_HSI						0
#define OSCILLATOR_HSE						1
#define OSCILLATOR_HSE_BYPASS				2
#define OSCILLATOR_PLL						3
#define OSCILLATOR_LSI						4
#define OSCILLATOR_LSE						5
#define OSCILLATOR_LSE_BYPASS				6

/*
 * @Oscillators_frequency
 */
#define HSI_FREQUENCY						8000000U
#define HSI_DIV2_FREQUENCY					4000000U
#define HSE_FREQUENCY						16000000U
/*
 * @SYSTEM_CLOCK_SWITCH
 */
#define SYSCLK_HSI							0
#define SYSCLK_HSE							1
#define SYSCLK_PLL							2

/*
 * @SYSTEM_CLOCK_SWITCH_STATUS
 */
#define SYSCLK_STATUS_HSI					0
#define SYSCLK_STATUS_HSE					1
#define SYSCLK_STATUS_PLL					2

/*
 * @HCLK_PRESCALER
 */
#define HPRE_SYSCLK_NO_DIV					0 // note that any other value between 0 and 8 means actually no sysclk division
#define HPRE_SYSCLK_DIV_2					8
#define HPRE_SYSCLK_DIV_4					9
#define HPRE_SYSCLK_DIV_8					10
#define HPRE_SYSCLK_DIV_16					11
#define HPRE_SYSCLK_DIV_64					12
#define HPRE_SYSCLK_DIV_128					13
#define HPRE_SYSCLK_DIV_256					14
#define HPRE_SYSCLK_DIV_512					15

/*
 * @APB1_PRESCALER
 */
#define PPRE1_SYSCLK_NO_DIV					0// note that any other value between 0 and 4 means actually no sysclk division
#define PPRE1_SYSCLK_DIV_2					4
#define PPRE1_SYSCLK_DIV_4					5
#define PPRE1_SYSCLK_DIV_8					6
#define PPRE1_SYSCLK_DIV_16					7

/*
 * @APB2_PRESCALER
 */
#define PPRE2_SYSCLK_NO_DIV					0// note that any other value between 0 and 4 means actually no sysclk division
#define PPRE2_SYSCLK_DIV_2					4
#define PPRE2_SYSCLK_DIV_4					5
#define PPRE2_SYSCLK_DIV_8					6
#define PPRE2_SYSCLK_DIV_16					7

/*
 * @PLL_Multiplier
 */

#define PLL_MULTIPLIER_2					0
#define PLL_MULTIPLIER_3					1
#define PLL_MULTIPLIER_4					2
#define PLL_MULTIPLIER_5					3
#define PLL_MULTIPLIER_6					4
#define PLL_MULTIPLIER_7					5
#define PLL_MULTIPLIER_8					6
#define PLL_MULTIPLIER_9					7
#define PLL_MULTIPLIER_10					8
#define PLL_MULTIPLIER_11					9
#define PLL_MULTIPLIER_12					10
#define PLL_MULTIPLIER_13					11
#define PLL_MULTIPLIER_14					12
#define PLL_MULTIPLIER_15					13
#define PLL_MULTIPLIER_16					15 // Note that the value of 14 also gives multiplier value of 16

/*
 * @PLLSRC
 */
#define PLLSRC_HSI_DIVIDED_BY_2				0
#define PLLSRC_HSI							1

#define PLLSRC_NUMBER_TO_SUBTRACT			2
#define PLLSRC_HSE							2
#define PLLSRC_HSE_DIVIDED_BY_2				3
#define PLLSRC_HSE_DIVIDED_BY_3				4
#define PLLSRC_HSE_DIVIDED_BY_4				5
#define PLLSRC_HSE_DIVIDED_BY_5				6
#define PLLSRC_HSE_DIVIDED_BY_6				7
#define PLLSRC_HSE_DIVIDED_BY_7				8
#define PLLSRC_HSE_DIVIDED_BY_8				9
#define PLLSRC_HSE_DIVIDED_BY_9				10
#define PLLSRC_HSE_DIVIDED_BY_10			11
#define PLLSRC_HSE_DIVIDED_BY_11			12
#define PLLSRC_HSE_DIVIDED_BY_12			13
#define PLLSRC_HSE_DIVIDED_BY_13			14
#define PLLSRC_HSE_DIVIDED_BY_14			15
#define PLLSRC_HSE_DIVIDED_BY_15			16

/*
 * @RCC_CR_FLAGS
 */
#define RCC_FLAG_PLLRDY						RCC_CR_PLLRDY
#define RCC_FLAG_HSIRDY						RCC_CR_HSIRDY
#define RCC_FLAG_HSERDY						RCC_CR_HSERDY
#define RCC_FLAG_LSIRDY						RCC_CSR_LSIRDY
#define RCC_FLAG_LSERDY						RCC_BDCR_LSERDY

/** Peripheral Clock Sources Definitions **/
#define RCC_RTC_CLOCKSOURCE_LSE				1
#define RCC_RTC_CLOCKSOURCE_LSI				2
#define RCC_RTC_CLOCKSOURCE_HSE_DIV32		3

/******************************************************************************************
 *								APIs supported by this driver
 *		 For more information about the APIs check the function definitions
 ******************************************************************************************/

/*
 * APIs to oscillators start, stop and wait until ready
 */

void RCC_StartHSI();
void RCC_StopHSI();

void RCC_StartHSE();
void RCC_StopHSE();

void RCC_StartLSI();
void RCC_StopLSI();

void RCC_StartLSE();
void RCC_StopLSE();

void RCC_StartLSE_BYPASS();
void RCC_StopLSE_BYPASS();

void RCC_StartHSE_BYPASS();
void RCC_StopHSE_BYPASS();

void RCC_StartPLL();
void RCC_StopPLL();

void RCC_WaitUntilHSI_Ready();
void RCC_WaitUntilHSE_Ready();
void RCC_WaitUntilPLL_Ready();
void RCC_WaitUntilLSI_Ready();
void RCC_WaitUntilLSE_Ready();

/*
 * Initialization of oscillators
 */

void RCC_HSI_Init();
void RCC_HSE_Init();
void RCC_HSE_BYPASS_Init();
void RCC_PLL_Init(uint8_t PLL_Multiplier, uint8_t PLLClockSource);
void RCC_LSE_Init();
void RCC_LSE_BYPASS_Init();
void RCC_LSI_Init();

/*
 * APIs to configure oscillators
 */

void RCC_ConfigurePLL(uint8_t PLL_Multiplier, uint8_t PLLClockSource);
void RCC_ConfigureHSI_Calibration(uint8_t calibrationVaule);
void RCC_ConfigureHSI_TrimmingValue(uint8_t trimmingValue);

/*
 * System clocks configuration and source switching
 */
void RCC_SetSystemClockSwitch(uint8_t clock);
void RCC_EnableCSS(uint8_t EnableOrDisable);
void RCC_ConfigureAPB1_Prescaler(uint8_t APB1_Prescaler);
void RCC_ConfigureAPB2_Prescaler(uint8_t APB2_Prescaler);
void RCC_ConfigureHCLK_Prescaler(uint8_t HCLK_Prescaler);

/*
 * Peripheral clocks configuration
 */
void RCC_ConfigurePeripheralClock(PeriperalClockSource_t *periphClock);
void RCC_SelectRTC_ClockSource(uint8_t RTC_ClockSource);

/*
 * Getters of clock values
 */
uint8_t RCC_GetSystemClockSwitch(void);
uint16_t RCC_GetHCLKPrescaler(void);
uint8_t RCC_GetAPB1Prescaler(void);
uint8_t RCC_GetAPB2Prescaler(void);
uint32_t RCC_GetSYSCLKValue(void);
uint32_t RCC_GetHCLKValue(void);
uint32_t RCC_GetPCLK1Value(void);
uint32_t RCC_GetPCLK2Value(void);
uint32_t RCC_GetPLLOutput();
uint32_t RCC_GetPLLSource();
uint8_t RCC_GetPLLMultiplier();

/*
 * IRQ Configuration and ISR handling
 */
void RCC_EnableInterrupt();
void RCC_IRQHandler();

#endif /* INC_STM32F303CC_RCC_H_ */
